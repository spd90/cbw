<?php
include 'include/functions.php';
$sec = new Security();
$link = mssql_connect(DB_ADDR, DB_USER, DB_PASS);
if (!$link) die('Could not connect to MSSQL database.');

$uid='';
$pass='';
$failed=false;

$user = $sec->secUser($_POST['username']);
$pass1 = $sec->secPass($_POST['password1']);
$pass2 = $sec->secPass($_POST['password2']);
$mail = $sec->checkEmail($_POST['email']);
$ip = $_SERVER["REMOTE_ADDR"];
//$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
$question = $_POST['question'];
$answer = $_POST['answer'];

if (isset($_POST['username'])) {
  if ($user == '' || strlen($user) < 6) {
	  $failed=true;
	  echo 'Username must be minimum 6 characters long and only (a-z, 0-9) characters are allowed!<br />';
	  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
  } else {
	$uid=$user;	
  }
}

if (isset($_POST['password1'])) {
  if ($pass1 == ''  || strlen($pass1) < 6) {
	  $failed=true;
	  echo 'Password must be minimum 6 characters long and only (a-z, 0-9) characters are allowed!<br />';
	  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
  } else {
	$pass=$pass1;	
  }
}

if (isset($_POST['password2'])) {
  if ($pass2 == ''  || strlen($pass2) < 6) {
	  $failed=true;
  } else {
    if ($pass2 != $pass1) {
	    $failed=true;
	    echo 'Passwords did not match.<br />';
		echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
    }
  }
}

if (isset($_POST['email'])) {
  if ($mail == '') {
	  $failed=true;
	  echo 'Please enter a valid email.<br />';
	  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
  } else {
	$email=$mail;	
  }
}

if (isset($_POST['question'])) {
  if ($question=='0') {
	  $failed=true;
	  echo 'Please select a question.<br />';
	  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
  } else {
	$sec_question=$_POST['question'];	
  }
}

 If (GetRegCount($ip) >= 3)
{
$failed=true;
	  echo 'You have too many registrations.<br />';
	  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
} 

if ($failed==true) {
  echo 'Registration Failed!';	
} else {
	
	if ($uid!='' && $pass!='') {
      $r=mssql_query('select count (*) from '.DB_ACC.'.dbo.cabal_auth_table where ID="'.$uid.'"');
      if (mssql_result($r,0,0)==0) {
        $r=mssql_query('exec '.DB_ACC.'.dbo.cabal_tool_registerAccount_Web "'.$uid.'","'.$pass.'","'.$email.'","'.$ip.'","'.$sec_question.'","'.$answer.'"');
        if ($r==false) {
  	      echo 'Something went wrong. Please try again!<br />';
		  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
        } else {
  	      echo 'Registration Successfull!';
		   header( 'Location: index.php#page_register-ok' ) ;
        }
        mssql_free_result($r);
        mssql_close($link);	
      } else {
	      echo 'Username already in used.<br />';
		  echo '<a href=javascript:history.go(-1) >Back to the register</a><hr />';
      }
    }
}

?>