<?php
    include 'include/functions.php';
    
    setcookie("username","", time()-1800);
    setcookie("usernum", "", time()-1800);
    setcookie("logined", "", time()-1800);
    setcookie("Hash", "", time()-1800);
    setcookie("ID", "", time()-1800);
    
    stdhead();
	sechead(logout);
    
    echo 'You have succesfully logged out!';
    
	secfoot();
    stdfoot();
    
    header( "refresh:0;url=index.php" );
    
?>